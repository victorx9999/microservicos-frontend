module.exports = {
    publicPath: process.env.VUE_APP_MODE === 'production' ? process.env.VUE_APP_BASE_ROUTE : '/',
    configureWebpack: {
        mode: process.env.VUE_APP_MODE === 'production' ? 'production' : 'development'
    },
    css: {
        loaderOptions: {
            less: {
                lessOptions: {
                    modifyVars: {
                        'primary-color': '#1DA57A',
                        'link-color': '#1DA57A',
                        'border-radius-base': '2px',
                    },
                    javascriptEnabled: true,
                },
            },
        },
    },
};