import Vue from 'vue'
import Vuex from 'vuex';
import VueAxios from 'vue-axios'
import Vuelidate from 'vuelidate'
import SimpleVueValidation from 'simple-vue-validator';
import router from './router'
import store from './store'
import axios from 'axios'
import App from './App.vue'
import Antd from 'ant-design-vue';
import 'ant-design-vue/dist/antd.css';
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'

Vue.config.productionTip = false
Vue.use(Antd)
Vue.use(VueAxios, axios)
Vue.use(Vuelidate)
Vue.use(Vuex)
Vue.use(SimpleVueValidation);

router.beforeResolve((to, from, next) => {
    if (to.name) {
        NProgress.start()
    }
    next()
})
  
router.afterEach(() => {
    NProgress.done()
})

axios.defaults.baseURL = process.env.VUE_APP_HOST || 'http://localhost:3000/api';
axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*';

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
