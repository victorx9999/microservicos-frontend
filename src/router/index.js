import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
// import store from '../store/index.js'
import Usuarios from '../views/Usuarios.vue'
import Setores from '../views/Setores.vue'
Vue.use(VueRouter)

    const routes = [
        {
            path: '/',
            name: 'Home',
            component: Home,
        },
        {
            path: '/usuarios',
            name: 'Usuarios',
            component: Usuarios,
        },
        {
            path: '/setores',
            name: 'Setores',
            component: Setores
        },
        {
            path: '/login',
            name: 'Login',
            component: () => import(/* webpackChunkName: "about" */ '../views/Login.vue'),
        }
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})



export default router
